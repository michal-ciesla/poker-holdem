import java.util.*;

public enum Pattern {
    ROYAL_FLUSH (10),
    STRAIGHT_FLUSH (9),
    FOUR_OF_A_KIND (8),
    FULL_HOUSE (7),
    FLUSH (6),
    STRAIGHT (5),
    THREE_OF_A_KIND (4),
    TWO_PAIR (3),
    ONE_PAIR (2),
    HIGH_CARD (1);

    private int patternWeight;

    Pattern (int patternWeight) {
        this.patternWeight = patternWeight;
    }

    public int getPatternWeight() {
        return patternWeight;
    }

    private static Card[] getAllCard(Table table, Player player) {
        int countCardOnTable = 0;
        int countCardPlayer = player.getCards().length;

        for(; countCardOnTable<table.getCards().length; countCardOnTable++) {
            if (table.getCardAtIndex(countCardOnTable) == null) {
                break;
            }
        }

        Card[] allCard = new Card[countCardOnTable + countCardPlayer];

        for(int i = 0; i< countCardOnTable; i++) {
            allCard[i] = table.getCardAtIndex(i);
        }

        for(int i = 0; i<countCardPlayer; i++) {
            allCard[i+countCardOnTable] = player.getCardAtIndex(i);
        }
        return allCard;
    }


    private static boolean checkRoyalFace(List<Card> cards) {
        int checkFace = 0;
        for(Card card : cards) {
            switch (card.getFace()) {
                case ACE:
                    checkFace++;
                    break;
                case KING:
                    checkFace++;
                    break;
                case QUEEN:
                    checkFace++;
                    break;
                case JACK:
                    checkFace++;
                    break;
                case TEN:
                    checkFace++;
                    break;
            }
        }
        if(checkFace == 5) {
            return true;
        }
        return false;
    }

    private static void getSameCardSuit(List<Card> clubsCard, List<Card> heartsCard,
                                        List<Card> diamondsCard, List<Card> spadesCard,
                                        Card[] allCard) {
        for(Card card: allCard) {
            switch (card.getSuit()) {
                case CLUBS:
                    clubsCard.add(card);
                    break;
                case HEARTS:
                    heartsCard.add(card);
                    break;
                case DIAMONDS:
                    diamondsCard.add(card);
                    break;
                case SPADES:
                    spadesCard.add(card);
            }
        }
    }

    private static boolean checkNextFiveCard(ArrayList<Card> allCard) {
        boolean[] findNextFiveCard = new boolean[13];
        int countNextFace = 0;

        for(Card card : allCard) {
            int index = card.getFace().getId()-2;
            findNextFiveCard[index] = true;
        }

        for(int i =0; i<findNextFiveCard.length; i++) {
            if(findNextFiveCard[i]) {
                countNextFace++;
            } else {
                countNextFace = 0;
            }
        }

        if(countNextFace >= 5) {
            return true;
        }
        return false;
    }

    //TODO try return Pattern not boolean
    //TODO check highest card in pattern

    public static boolean isRoyalFlush(Table table, Player player) {
        Card[] allCard = getAllCard(table, player);

        ArrayList<Card> clubsCard = new ArrayList<Card>();
        ArrayList<Card> heartsCard = new ArrayList<Card>();
        ArrayList<Card> diamondsCard = new ArrayList<Card>();
        ArrayList<Card> spadesCard = new ArrayList<Card>();

        getSameCardSuit(clubsCard, heartsCard, diamondsCard, spadesCard, allCard);

        if(!(clubsCard.size() < 4)) {
            checkRoyalFace(clubsCard);
        } else if(!(heartsCard.size() <4)) {
            checkRoyalFace(heartsCard);
        } else if(!(diamondsCard.size() <4)) {
            checkRoyalFace(diamondsCard);
        } else if(!(spadesCard.size() < 4)) {
            checkRoyalFace(spadesCard);
        }
        return false;
    }

    public static boolean isStraightFlush(Table table, Player player) {
        Card[] allCard = getAllCard(table, player);

        ArrayList<Card> clubsCard = new ArrayList<Card>();
        ArrayList<Card> heartsCard = new ArrayList<Card>();
        ArrayList<Card> diamondsCard = new ArrayList<Card>();
        ArrayList<Card> spadesCard = new ArrayList<Card>();

        getSameCardSuit(clubsCard, heartsCard, diamondsCard, spadesCard, allCard);

        if(clubsCard.size() >= 5) {
            checkNextFiveCard(clubsCard);
        } else if(heartsCard.size() >= 5) {
            checkNextFiveCard(heartsCard);
        } else if(diamondsCard.size() >= 5) {
            checkNextFiveCard(diamondsCard);
        } else if(spadesCard.size() >= 5) {
            checkNextFiveCard(spadesCard);
        }
        return false;
    }

    public static boolean isFourOfKind(Table table, Player player) {
        Card[] allCard = getAllCard(table, player);
        HashMap<Face, Integer> countFace = new HashMap<Face, Integer>();

        for(Card card: allCard) {
            countFace.put(card.getFace(),0);
        }

        for(Card card: allCard) {
            int count = countFace.get(card.getFace());
            countFace.put(card.getFace(),++count);
        }

        for(int count: countFace.values()) {
            if(count >=4) {
                return true;
            }
        }
        return false;
    }

    public static boolean isFullHouse(Table table, Player player) {
        Card[] allCard = getAllCard(table, player);
        HashMap<Face, Integer> countFace = new HashMap<Face, Integer>();
        int countPair =0;
        int countThree =0;

        for(Card card: allCard) {
            countFace.put(card.getFace(),0);
        }

        for(Card card: allCard) {
            int count = countFace.get(card.getFace());
            countFace.put(card.getFace(),++count);
        }

        for(int count: countFace.values()) {
            if(count ==2) countPair++;
            if(count >= 3) countThree++;
        }

        if(countPair>=1 && countThree>=1) {
            return true;
        }

        return false;
    }

    public static boolean isFlush(Table table, Player player) {
        Card[] allCard = getAllCard(table, player);
        HashMap<Suit, Integer> countSuit = new HashMap<Suit, Integer>();

        for(Card card: allCard) {
            countSuit.put(card.getSuit(), 0);
        }
        for(Card card: allCard) {
            int count = countSuit.get(card.getSuit());
            countSuit.put(card.getSuit(), ++count);
        }

        for(int count: countSuit.values()) {
            if(count >=5) {
                return true;
            }
        }
        return false;
    }

    public static boolean isStraight(Table table, Player player) {
        Card[] allCard = getAllCard(table, player);
        boolean[] findNextFiveCard = new boolean[13];
        int countNextFace = 0;

        for(Card card : allCard) {
            int index = card.getFace().getId()-2;
            findNextFiveCard[index] = true;
        }

        for(int i =0; i<findNextFiveCard.length; i++) {
            if(findNextFiveCard[i]) {
                countNextFace++;
            } else {
                countNextFace = 0;
            }
        }

        if(countNextFace >= 5) {
            return true;
        }

        return false;
    }

    public static boolean isThreeOfKind(Table table, Player player) {
        Card[] allCard = getAllCard(table, player);
        HashMap<Face, Integer> countFace = new HashMap<Face, Integer>();

        for(Card card: allCard) {
            countFace.put(card.getFace(), 0);
        }

        for(Card card: allCard) {
            int count = countFace.get(card.getFace());
            countFace.put(card.getFace(), ++count);
        }

        for(int count: countFace.values()) {
            if(count == 3) {
                return true;
            }
        }
        return false;
    }

    public static boolean isTwoPair(Table table, Player player) {
        Card[] allCard = getAllCard(table, player);
        HashMap<Face, Integer> countFace = new HashMap<Face, Integer>();
        int countPattern =0;

        for(Card card: allCard) {
            countFace.put(card.getFace(), 0);
        }

        for(Card card: allCard) {
            int count = countFace.get(card.getFace());
            countFace.put(card.getFace(), ++count);
        }

        for(int count: countFace.values()) {
            if(count ==2) countPattern++;
        }

        if(countPattern >= 2) {
            return true;
        }
        return false;
    }

    public static boolean isOnePair(Table table, Player player) {
        Card[] allCard = getAllCard(table, player);
        HashMap<Face, Integer> countFace = new HashMap<Face, Integer>();

        for(Card card: allCard) {
            countFace.put(card.getFace(), 0);
        }

        for(Card card: allCard) {
            int count = countFace.get(card.getFace());
            countFace.put(card.getFace(), ++count);
        }

        for(int count: countFace.values()) {
            if(count == 2) {
                return true;
            }
        }
        return false;
    }

    public static boolean isHighCard(Table table, Player player) {
        Card[] allCard = getAllCard(table, player);
        //TODO implements
        return true;
    }
}
