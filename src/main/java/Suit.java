public enum Suit {
    HEARTS ("Hearts"),
    SPADES ("Spades"),
    DIAMONDS ("Diamonds"),
    CLUBS ("Clubs");

    private String shortSuit;

    Suit(String shortSuit) {
        this.shortSuit = shortSuit;
    }

    @Override
    public String toString() {
        return shortSuit;
    }

}
