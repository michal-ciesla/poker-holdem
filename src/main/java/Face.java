public enum Face {
    TWO ("2", 2),
    THREE ("3", 3),
    FOUR ("4", 4),
    FIVE ("5", 5),
    SIX ("6", 6),
    SEVEN ("7", 7),
    EIGHT ("8", 8),
    NINE ("9", 9),
    TEN ("10", 10),
    JACK ("Jack", 11),
    QUEEN ("Queen", 12),
    KING ("King", 13),
    ACE ("Ace", 14);

    private String simpleName;
    private int id;

    Face(String simpleName, int id) {
        this.simpleName = simpleName;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return simpleName;
    }
}
