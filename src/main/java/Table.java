import java.util.Arrays;

public class Table {
    private Card[] cardsOnTable;

    protected static final int MAX_CARD = 5;
    static final int INDEX_FLOP_CARD = 2;
    static final int INDEX_TURN_CARD = 3;
    static final int INDEX_RIVER_CARD = 4;

    public Table() {
        cardsOnTable = new Card[5];
    }

    public Card[] getCards() {
        return cardsOnTable;
    }

    public Card getCardAtIndex(int index) {
        return cardsOnTable[index];
    }

    public void setCardAtIndex(Card card, int index) {
        cardsOnTable[index] = card;
    }

    public void reset() {
        Arrays.fill(cardsOnTable,null);
    }

    @Override
    public String toString() {
        return Arrays.toString(cardsOnTable);
    }


}
