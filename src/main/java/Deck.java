import java.util.Arrays;
import java.util.Random;

public class Deck {
    private Face[] faces;
    private Suit[] suites;
    private Card[] deck;
    private Random random;
    protected static final int TOTAL_CARD = 52;

    public Deck() {
        faces = Face.values();
        suites = Suit.values();
        deck = new Card[TOTAL_CARD];
        random = new Random();
        for(int i =0; i<TOTAL_CARD; i++) {
            deck[i] = new Card(faces[i%13], suites[i/13]);
        }
    }

    public Card getCard(int index) {
        return deck[index];
    }

    public Card[] getDeck() {
        return deck;
    }

    public void shuffle() {
        for(int i = 0; i<TOTAL_CARD; i++) {
            int j = random.nextInt(TOTAL_CARD);
            Card tmpCard = deck[i];
            deck[i] = deck[j];
            deck[j] = tmpCard;
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(deck);
    }
}
