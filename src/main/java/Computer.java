import java.util.Arrays;
import java.util.Random;

public class Computer implements Player {

    private Card[] cardsOnHand;
    private boolean fold;
    private boolean call;
    private int account;
    private int jackpot;
    private int patternWeight;
    private int newBid;

    public Computer() {
        cardsOnHand = new Card[Player.MAX_CARD];
        fold = false;
        call = false;
        account = Player.START_ACCOUNT;
        patternWeight = 0;
    }

    public Card[] getCards() {
        return cardsOnHand;
    }

    public Card getCardAtIndex(int index) {
        return cardsOnHand[index];
    }

    public void setCardAtIndex(Card card, int index) {
        cardsOnHand[index] = card;
    }

    public void fold() {
        fold = true;
    }

    public void call(Game game) {
        if (jackpot < game.getMaxJackpot()) {
            int diff = game.getMaxJackpot() - jackpot;
            jackpot = jackpot + diff;
            account = account - diff;
        }
        call = true;
    }

    public void raise(int newBid) {
        jackpot = jackpot + newBid;
        account = account - newBid;
    }

    public void check() {
        //TODO implements
    }

    public int auctionChoice(Table table, Game game) {
        //TODO better AI
        Random random = new Random();
        Pattern highestPattern = checkHighestPattern(table);
        if(highestPattern.getPatternWeight()>patternWeight) {
            patternWeight = highestPattern.getPatternWeight();
            newBid = random.nextInt(64);
            return 2;
        } else if(highestPattern.getPatternWeight()==patternWeight){
            newBid = 0;
            return 3;
        }
        newBid = 0;
        return 1;
    }

    public int getBid() {
        return newBid;
    }


    public int getJackpot() {
        return jackpot;
    }

    public void resetJackpot() {
        jackpot =0;
        patternWeight = 0;
    }

    public void updateAccount(int value) {
        account = account + value;
    }

    public int getAccount() {
        return account;
    }

    public void resetCall() {
        call = false;
    }

    public boolean getCall() {
        return call;
    }

    public boolean getFold() {
        return fold;
    }

    public void resetFold() {
        fold = false;
    }

    @Override
    public String toString() {
        return "My cards on hand: " + Arrays.toString(cardsOnHand);
    }
}
