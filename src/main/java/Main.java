import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("How many players: ");
        int numberOfPlayers = scanner.nextInt();
/*
        System.out.print("How many users: ");
        int numberOfUsers = scanner.nextInt();

        while(numberOfUsers> numberOfPlayers) {
            System.out.println("Wrong number. Number of users must be less than number of players.");
            System.out.println("Try again!");
            System.out.println();
            System.out.print("How many users: ");
            numberOfUsers = scanner.nextInt();
        }

        System.out.print("How many computers: ");
        int numberOfComputers = scanner.nextInt();

        while(numberOfComputers+ numberOfUsers> numberOfPlayers || numberOfComputers+ numberOfUsers<numberOfPlayers) {
            System.out.println("Wrong number. Number of computers nad users must be equals number of players.");
            System.out.println("Try again!");
            System.out.println();
            System.out.print("How many computers: ");
            numberOfComputers = scanner.nextInt();
        }
*/
        Game game = new Game(numberOfPlayers, 1, numberOfPlayers - 1);

        User userPlayer = (User) game.getPlayers()[0];
        Computer[] computerPlayer = new Computer[numberOfPlayers - 1];

        for (int i = 1; i < numberOfPlayers; i++) {
            computerPlayer[i - 1] = (Computer) game.getPlayers()[i];
        }
        do {
            System.out.println("Ok, let's start the game!");
            System.out.println();
            System.out.println("Your account: " + userPlayer.getAccount());

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println();
            System.out.println("Deal card...");
            game.dealPlayerCard();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println();
            System.out.println("Card on your hand: " + userPlayer.toString());
            System.out.println();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Start auction!");
            auction(numberOfPlayers, game, userPlayer, computerPlayer);

            System.out.println("Deal flop...");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            game.dealFlop();

            System.out.println();
            System.out.println("Card on your hand: " + userPlayer.toString());
            System.out.println("Card on the table: " + game.getTable().toString());
            System.out.println();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Start auction!");
            auction(numberOfPlayers, game, userPlayer, computerPlayer);

            System.out.println("Deal turn...");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            game.dealTurn();

            System.out.println();
            System.out.println("Card on your hand: " + userPlayer.toString());
            System.out.println("Card on the table: " + game.getTable().toString());
            System.out.println();

            System.out.println("Start auction!");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            auction(numberOfPlayers, game, userPlayer, computerPlayer);

            System.out.println("Deal river...");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            game.dealRiver();

            System.out.println();
            System.out.println("Card on your hand: " + userPlayer.toString());
            System.out.println("Card on the table: " + game.getTable().toString());
            System.out.println();

            System.out.println("Start auction!");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            auction(numberOfPlayers, game, userPlayer, computerPlayer);

            System.out.println("And the winner is....");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Player winPlayer = game.whoPatternIsHighest();
            game.updatePlayerAccount(winPlayer);

            System.out.println("Do yout want the next turn? (y or n)");
        }
        while (scanner.next().equals("y") && userPlayer.getAccount()>0);
        System.out.println("You break game or have account less than 0");
        System.out.println();
        System.out.println();
        System.out.println("END GAME");
    }

    private static void auction(int numberOfPlayers, Game game, User userPlayer, Computer[] computerPlayer) {
        int everyCallOrFold = 0;
        int playerChoice = 0;
        Player[] players = game.getPlayers();

        for (Player player : players) {
            player.resetCall();
        }

        while (everyCallOrFold != numberOfPlayers) {
            everyCallOrFold = 0;

            playerChoice(game, userPlayer, playerChoice);

            System.out.println("Computer turn...");
            System.out.println();
            for (int i = 1; i < numberOfPlayers; i++) {
                int computerChoice = computerPlayer[i - 1].auctionChoice(game.getTable(), game);
                game.auction(i, computerChoice, computerPlayer[i - 1].getBid());
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < computerPlayer.length; i++) {
                System.out.println("Computer " + (i + 1) + " Jackpot: " + computerPlayer[i].getJackpot() +" Call: " +computerPlayer[i].getCall() + " Fold: " +computerPlayer[i].getFold());
            }

            for (int i = 0; i < game.getPlayers().length; i++) {
                if (players[i].getFold() || players[i].getCall()) {
                    everyCallOrFold++;
                }
            }
            System.out.println();
            System.out.println("My jackpot: " + userPlayer.getJackpot());
            System.out.println("My account: " + userPlayer.getAccount());
            System.out.println();
            System.out.println("All game jackpot: " + game.getAllJackpot());
            System.out.println();
        }
    }

    private static void playerChoice(Game game, User userPlayer, int playerChoice) {
        int newBid = 0; //TODO if newBid is max account then can not be less than zero

        if (!userPlayer.getCall() && !userPlayer.getFold()) {
            System.out.println("What you want?");
            System.out.println(" 1. Fold");
            System.out.println(" 2. Raise");
            System.out.println(" 3. Call");
            System.out.print("Choose number: ");
            playerChoice = scanner.nextInt();

            while (playerChoice > 3 || playerChoice < 1) {
                System.out.println("Wrong choice. Try again!");
                System.out.println();
                System.out.println("What you want?");
                System.out.println(" 1. Fold");
                System.out.println(" 2. Raise");
                System.out.println(" 3. Call");
                System.out.print("Choose number: ");
                playerChoice = scanner.nextInt();
            }

            if (playerChoice == 2) {
                System.out.print("How much do you want raise? ");
                newBid = scanner.nextInt();

                while (newBid > userPlayer.getAccount() || newBid <= 0) {
                    System.out.println("Wrong value. Your value must be less than your account");
                    System.out.println("Try again!");
                    System.out.println();
                    System.out.print("How much do you want raise? ");
                    newBid = scanner.nextInt();
                }
            } else {
                newBid = 0;
            }
        }
        game.auction(0, playerChoice, newBid);
    }
}
