public interface Player {

    static int MAX_CARD = 2;
    static int START_ACCOUNT = 1000;

    public Card[] getCards();
    public Card getCardAtIndex(int index);
    public void setCardAtIndex(Card card, int index);

    public default Pattern checkHighestPattern(Table table) {
        if(Pattern.isRoyalFlush(table, this)) {
            return Pattern.ROYAL_FLUSH;
        } else if(Pattern.isStraightFlush(table, this)) {
            return Pattern.STRAIGHT_FLUSH;
        } else if(Pattern.isFourOfKind(table, this)) {
            return Pattern.FOUR_OF_A_KIND;
        } else if(Pattern.isFullHouse(table, this)) {
            return Pattern.FULL_HOUSE;
        } else if(Pattern.isFlush(table, this)) {
            return Pattern.FLUSH;
        } else if(Pattern.isStraight(table, this)) {
            return Pattern.STRAIGHT;
        } else if(Pattern.isThreeOfKind(table, this)) {
            return Pattern.THREE_OF_A_KIND;
        } else if(Pattern.isTwoPair(table, this)) {
            return Pattern.TWO_PAIR;
        } else if(Pattern.isOnePair(table, this)) {
            return Pattern.ONE_PAIR;
        } else if(Pattern.isHighCard(table, this)) {
            return Pattern.HIGH_CARD;
        }
        return null;
    }
    public void fold();
    public void call(Game game);
    public void raise(int newBid);
    public void check();
    public int getJackpot();
    public void resetJackpot();
    public int getAccount();
    public void updateAccount(int value);
    public void resetCall();
    public boolean getCall();
    public boolean getFold();
    public void resetFold();
}
