public class Game {
    private Player[] players;
    private Table table;
    private Deck deck;
    private int cardCounter;
    private int maxJackpot = 0;

    public Game(int numberOfPlayers, int howManyUsers, int howManyComputers) {
        players = new Player[numberOfPlayers];
        table = new Table();
        cardCounter = 0;

        //TODO make random Players array
        for (int i = 0; i < numberOfPlayers; i++) {
            if (howManyUsers > 0) {
                players[i] = new User();
                howManyUsers--;
            } else if (howManyComputers > 0) {
                players[i] = new Computer();
                howManyComputers--;
            }
        }
        deck = new Deck();
        deck.shuffle();
    }

    public Player[] getPlayers() {
        return players;
    }

    public Card[] getTableCard() {
        return table.getCards();
    }

    public void dealPlayerCard() {
        for (int i = 0; i < Player.MAX_CARD; i++) {
            for (int j = 0; j < players.length; j++) {
                players[j].setCardAtIndex(deck.getCard(cardCounter++), i);
            }
        }
    }

    public void dealFlop() {
        for (int i = 0; i <= Table.INDEX_FLOP_CARD; i++) {
            table.setCardAtIndex(deck.getCard(cardCounter++), i);
        }
    }

    public void dealTurn() {
        table.setCardAtIndex(deck.getCard(cardCounter++), Table.INDEX_TURN_CARD);
    }

    public void dealRiver() {
        table.setCardAtIndex(deck.getCard(cardCounter++), Table.INDEX_RIVER_CARD);
    }

    public void auction(int playerIndex, int playerChoice, int newBid) {
        if (!players[playerIndex].getFold()) {

            switch (playerChoice) {
                case 1:
                    players[playerIndex].fold();
                    break;
                case 2:
                    players[playerIndex].raise(newBid);
                    break;
                case 3:
                    players[playerIndex].call(this);
                    break;
                default:
                    System.out.println("Wrong choice! Try again");
            }
            int playerJackpot = players[playerIndex].getJackpot();
            if(playerJackpot>maxJackpot) {
                maxJackpot = playerJackpot;
            }
        }
    }

    public void showCard() {
        System.out.println("Card on the table: ");
        for(Card card: table.getCards()) {
            System.out.println(card.toString());
        }

        for (int i = 0; i < players.length; i++) {
            System.out.println("Player " + (i + 1) + " has card: ");
            for (Card card : players[i].getCards()) {
                System.out.println(card.toString());
            }
        }
    }

    public Player whoPatternIsHighest() {
        int patternWeight = 0;
        int indexPlayer = -1;
        for(int i = 0; i< players.length; i++) {
            System.out.print("Player " + (i+1) + ": ");
            Pattern paterrn = players[i].checkHighestPattern(table);
            boolean playerFold = players[i].getFold();
            System.out.println(" You have " + paterrn.toString());
            if(paterrn.getPatternWeight() > patternWeight && !playerFold) {
                indexPlayer = i;
                patternWeight = paterrn.getPatternWeight();
            }
            //TODO add situation when two players win
            //TODO when two players have the same patterns, win players with high card in this pattern.
        }
        System.out.println();
        System.out.println("Player " +(indexPlayer+1) + " win!");
        return players[indexPlayer];
    }

    public void updatePlayerAccount(Player player) {
        player.updateAccount(getAllJackpot());
        for(Player p: players) {
            p.resetJackpot();
            p.resetFold();
        }
        table.reset();
    }

    public int getMaxJackpot() {
        return maxJackpot;
    }

    public int getAllJackpot() {
        int allJackpot = 0;
        for(int i =0; i<players.length;i++) {
            allJackpot = allJackpot + players[i].getJackpot();
        }
        return allJackpot;
    }

    public Table getTable() {
        return table;
    }
}
