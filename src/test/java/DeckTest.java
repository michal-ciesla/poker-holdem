import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class DeckTest {

    static Face[] faces;
    static Suit[] suites;
    Card[] testDeck;

    @BeforeAll
    static void setUp() {
        faces = Face.values();
        suites = Suit.values();
    }

    @BeforeEach
    void createTestDeck() {
        Card[] testDeck = new Card[Deck.TOTAL_CARD];
        for(int i =0; i<Deck.TOTAL_CARD; i++) {
            testDeck[i] = new Card(faces[i%13], suites[i/13]);
        }
    }
    @Test
    void shouldCreateDeck() {
        Deck deck = new Deck();
        assertEquals(Arrays.toString(testDeck), Arrays.toString(deck.getDeck()));
    }

    @Test
    void shouldShuffleDeck() {
        Deck deck = new Deck();
        deck.shuffle();
        System.out.println(deck.toString());
        assertNotEquals(Arrays.toString(testDeck), Arrays.toString(deck.getDeck()));
    }

}