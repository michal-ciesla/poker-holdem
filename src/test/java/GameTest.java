import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {
    Game game;

    @BeforeEach
    void setUp() {
        game = new Game(4, 1, 3);
    }

    @Test
    void shoulMakeInstanceOfGame() {
        assertEquals(game.getPlayers().length, 4);
    }

    @Test
    void shoulMakeThreeComputerPlayer() {
        assertTrue(game.getPlayers()[2] instanceof Computer);
    }

    @Test
    void shoulMakeOneComputerPlayer() {
        assertTrue(game.getPlayers()[0] instanceof Player);
    }

    @Test
    void shouldDealCardForPlayers() {
        game.dealPlayerCard();
        assertNotNull(game.getPlayers()[2].getCardAtIndex(1));
    }

    @Test
    void shouldDealFlopCard() {
        game.dealFlop();
        System.out.println("Table: " + Arrays.toString(game.getTableCard()));
        assertNotNull(game.getTableCard()[2]);
    }

    @Test
    void shouldDealTurnCard() {
        game.dealFlop();
        game.dealTurn();
        System.out.println("Table: " + Arrays.toString(game.getTableCard()));
        assertNotNull(game.getTableCard()[3]);
    }

    @Test
    void shouldDealRiver() {
        game.dealFlop();
        game.dealTurn();
        game.dealRiver();
        System.out.println("Table: " + Arrays.toString(game.getTableCard()));
        assertNotNull(game.getTableCard()[4]);
    }

    @Test
    void shouldRaisePlayerInAuction() {
        game.auction(0, 2, 10);
        game.auction(1,2, 20);
        assertEquals(30, game.getAllJackpot());
    }

    @Test
    void shouldFoldSecondPlayer() {
        game.auction(0, 2, 10);
        game.auction(1, 1, 0);
        assertTrue(game.getPlayers()[1].getFold());
    }

    @Test
    void shouldCallThirdPlayer() {
        game.auction(0,2, 20);
        game.auction(1, 2, 10);
        game.auction(2,3,0);
        assertTrue(game.getPlayers()[2].getCall());
        assertEquals(50, game.getAllJackpot());
    }

    @Test
    void shouldCallThirdPlayer2() {
        game.auction(0,2, 30);
        game.auction(1, 2, 20);
        game.auction(2,3,0);
        assertEquals(30, game.getPlayers()[2].getJackpot());
        assertEquals(970, game.getPlayers()[2].getAccount());
    }

    @Test
    void wrongChoiceInAuction() {
        game.auction(1, 'e', 20);
    }

    @Test
    void shouldShowAllCard() {
        game.dealPlayerCard();
        game.dealFlop();
        game.dealTurn();
        game.dealRiver();
        game.showCard();
    }

    @Test
    void shouldReturnPlayerWithHighestPattern() {
        game.dealPlayerCard();
        game.dealFlop();
        game.dealTurn();
        game.dealRiver();
        game.showCard();
        game.whoPatternIsHighest();
    }

}