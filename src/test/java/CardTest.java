import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {

    @Test
    void shouldCreateCard() {
        Card card = new Card(Face.ACE, Suit.CLUBS);
        assertEquals("A of Clubs", card.toString());
    }

}